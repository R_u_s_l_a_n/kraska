var menu = document.getElementsByClassName('menu')[0];
var fon = document.getElementsByClassName('fon')[0];
var wrapper = document.getElementsByClassName('wrapper')[0];
var call = document.getElementsByClassName('phone-action')[0];
var close = document.getElementById('close');
var pop = document.getElementsByClassName('popup')[0];
var form = document.getElementsByClassName('form')[0];
var err_box = document.getElementsByClassName('err-box')[0];
var form__inp_input = document.getElementsByClassName('form__inp-input')[1];
var  getDiscount = document.getElementById('getDiscount');
var input_phone = document.getElementsByTagName('input')[1];
var client_w = document.body.clientWidth;
var question = document.getElementsByClassName('question')[0];
var answer = document.getElementsByClassName('answer')[0];

gamb.onclick = function(){
menu.style.right = 0;
wrapper.style.display = 'flex';
wrapper.style.zIndex = 7;
close.style.display = 'none';
}

 end.onclick = function(){
    wrapper.style.display = 'none';
    menu.style.right = '-301px';
    close.style.display = '';
    wrapper.style.zIndex = 8;
 }

 call.onclick = function() {
    
   pop.style.display = 'flex';
   pop.style.animation = 'show  .4s ease-in forwards';
    wrapper.style.display = 'flex';
 }
 close.onclick = function() {
   
   pop.style.animation = 'hide  .4s ease-in forwards';
   del_err_class();

   setTimeout(()=>{
      pop.style.display = 'none';
     wrapper.style.display = 'none';
   }, 300);
   
   if(answer.style.display == 'flex'){
    close_answer();
   }
  
 }

 function add_err_class(){
  form__inp_input.classList.add('err');
  err_box.style.display = 'flex';
 }

 function set_block_height(x,y){
  pop.style.height = x;
  form.style.height = y;
 }

function get_window_width(a,b,c,d){
  if( client_w >= 1200){
    set_block_height(a,b);         
  }

  if( (client_w >= 600)&&(client_w <= 1200)){
    set_block_height(c,d);            
  } 
}

 function validate_form()
   {
     valid = true;
   
           if (( input_phone.value == "" ) || (isNaN(input_phone.value)))
           {
              add_err_class();
              valid = false;
              get_window_width('680px','580px','700px','630px');
        }
           return valid;
   }

   function del_err_class(){
    form__inp_input.classList.remove('err');
    err_box.style.display = 'none';
   }

function data_send(){
  question.style.display = 'none';
  answer.style.display = 'flex';
  get_window_width('400px','320px','450px','380px');
}

function close_answer(){
  question.style.display = 'flex';
  answer.style.display = 'none';
  get_window_width('','','','');
  del_err_class();
  var input = document.getElementsByTagName('input');
for(i=0; i<input.length; i++){
  input[i].value = '';
}
  let event = new Event("click");
  close.dispatchEvent(event);
}

 getDiscount.onclick = function(){
  var res = validate_form();
 
  if(res){
    console.log(res);
    $.ajax({
        url: "./handler.php", 
        type: "post",
        
        success: function(data){
           // $('.messages').html(data.result); 
        }
    });
    data_send();
  } else{
    return false;
  }

}

answer.onclick = close_answer;